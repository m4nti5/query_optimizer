#include "query.h"

query::query(){
}

int query::num_relations(){
	return this->_relations.size();
}

string query::get_relation(int i){
	return this->_relations[i];
}

void query::add_relation(string r){
	this->_relations.push_back(r);
}

void query::add_operation(string table, COMPARISION op, string column, VALUE_TYPE &val){
	TABLE_OPERATION to;
	to.column = column;
	to.op = op;
	to.v = val;
	this->_operations[table].push_back(to);
}
	
void query::get_operation_info(string table, list<TABLE_OPERATION>::iterator& begin, list<TABLE_OPERATION>::iterator& end){
	map<string, list<TABLE_OPERATION> >::iterator it = this->_operations.find(table);
	if(it != this->_operations.end()){
		begin = (*it).second.begin();
		end = (*it).second.end();
	}
}

void query::add_projection_field(string f){
	this->_projection.push_back(f);
}

ostream& query::print(ostream &os){
	os << "SELECT " << endl << "\t";
	bool first = true;
	list<string>::iterator it;
	for(it = this->_projection.begin(); it != this->_projection.end(); ++it){
		if(!first){
			os << ", ";
		}else{
			first = false;
		}
		os << *it;
	}
	os << endl;
	os << "FROM" << endl << "\t";
	first = true;
	vector<string>::iterator itr;
	for(itr = this->_relations.begin(); itr != this->_relations.end(); ++itr){
		if(!first){
			os << ", ";
		}else{
			first = false;
		}
		os << *itr;
	}
	os << endl;
	os << "WHERE";
	map<string, list<TABLE_OPERATION> >::iterator ito;
	for(ito = this->_operations.begin(); ito != this->_operations.end(); ++ito){
		os << endl << "\t";
		list<TABLE_OPERATION>::iterator begin, end;
		get_operation_info((*ito).first, begin, end);
		bool first = true;
		for(;begin != end; begin++){
			if(first){
				first = false;
			}else{
				os << " AND ";
			}
			os << (*ito).first << "." << *begin;
		}
	}
	return os << endl;
}