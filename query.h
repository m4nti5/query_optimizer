#ifndef QUERY_H
#define QUERY_H

#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <string>

using namespace std;

typedef enum COMPARISION{
	MINOR,
	MAYOR,
	EQUAL
}COMPARISION;

typedef enum COLUMN_TYPE{
	CONSTANT,
	COLUMN
}COLUMN_TYPE;

typedef struct VALUE_TYPE{
	COLUMN_TYPE type;
	string value;
	string table;
}VALUE_TYPE;

typedef struct TABLE_OPERATION{
	string column;
	COMPARISION op;
	VALUE_TYPE v;
}TABLE_OPERATION;

class query{
	public:
		query();
		int num_relations();
		string get_relation(int);
		void add_relation(string);
		void add_operation(string, COMPARISION, string, VALUE_TYPE&);
		void get_operation_info(string, list<TABLE_OPERATION>::iterator&, list<TABLE_OPERATION>::iterator&);
		void add_projection_field(string);
		ostream& print(ostream &os);

	private:
		vector<string> _relations;
		map<string, list<TABLE_OPERATION> > _operations;
		list<string> _projection;
};

inline ostream& operator <<(ostream& os, query& q){
	return q.print(os);
}

inline ostream& operator <<(ostream& os, COMPARISION& c){
	switch(c){
		case MINOR:
			os << '<';
			break;
		case MAYOR:
			os << '>';
			break;
		case EQUAL:
			os << '=';
			break;
	}
	return os;
}

inline ostream& operator <<(ostream& os, TABLE_OPERATION& to){
	os << to.column << " " << to.op << " ";
	switch(to.v.type){
		case COLUMN:
			os << to.v.table << ".";
			break;
		default:
			break;
	}
	return os << to.v.value;
}

#endif
