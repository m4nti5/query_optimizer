#ifndef INDEX_H
#define INDEX_H
#include <list>
#include <iostream>
#include <string>

using namespace std;

typedef enum index_type{
	BTREE,
	HASH
}index_type;

typedef enum index_order{
	CLUSTERED,
	UNCLUSTERED
}index_order;

class index{
	public:
		string id;
		
		index();
		index& operator=(const index&);
		index_type get_type()const;
		index_order get_order()const;
		string get_first_field();
		void get_fields(list<string>::const_iterator&, list<string>::const_iterator&)const;
		int get_number_keys()const;
		int get_max_value()const;
		int get_min_value()const;
		int get_num_pages()const;
		int get_tuples_per_pages()const;
		void set_type(index_type);
		void set_order(index_order);
		void add_field(string);
		void set_fields(list<string>::const_iterator&, list<string>::const_iterator&);
		void set_number_keys(int);
		void set_max_value(int);
		void set_min_value(int);
		void set_num_pages(int);
		void set_tuples_per_pages(int);
		ostream& print(ostream&);

	private:
		index_type _type;
		index_order _order;
		int _number_keys;
		int _max_value;
		int _min_value;
		int _num_pages;
		int _tuples_per_pages;
		list<string> _fields;
};

inline ostream& operator << (ostream &os, index_type &t){
	switch(t){
		case BTREE:
			os << "BTREE";
			break;
		case HASH:
			os << "HASH";
			break;
	}
	return os;
}

inline ostream& operator << (ostream &os, index_order &o){
	switch(o){
		case CLUSTERED:
			os << "CLUSTERED";
			break;
		case UNCLUSTERED:
			os << "UNCLUSTERED";
			break;
	}
	return os;
}

inline ostream& operator << (ostream &os, index &ind){
	return ind.print(os);
}

#endif