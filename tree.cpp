#include <iostream>
#include <sstream>
#include <stdio.h>
#include "tree.h"

using namespace std;

tree::tree(){
	this->_left = NULL;
	this->_right = NULL;
	this->_operator = NONE;
	this->_relation = "";
	this->cost_so_far = 0;
	this->num_tuples = 0; // result cardinality
	this->tuple_size = 0; // new tuple size, sum in case of join
}

tree::tree(const tree& src){
	copy(this, &src);
}

tree::~tree(){
	if(this->_left)
		delete(this->_left);
	if(this->_right)
		delete(this->_right);
}

bool tree::is_ordered(string f, catalog &cat){
	if(this->_operator == SELECTION){
		string s1, index;
		stringstream ss;
		ss << this->selection_used;
		ss >> s1 >> index;
		return cat.has_ordered_index(this->_left->get_relation(), index, f);
	}
	return false;
}

bool tree::is_node(){
	return this->_operator != NONE;
}

void tree::get_fields(list<string>::const_iterator& init, list<string>::const_iterator& end) const{
	init = this->_fields.begin();
	end = this->_fields.end();
}

operators tree::get_operator() const{
	return this->_operator;
}

string tree::get_relation() const{
	return this->_relation;
}

string tree::get_selection_relation() const{
	if(this->_operator == SELECTION)
		return this->_right->get_relation();
	return this->get_relation();
}

void tree::add_field(string f){
	this->_fields.push_back(f);
}

void tree::set_fields(list<string>::const_iterator& begin, list<string>::const_iterator& end){
	this->_fields.clear();
	for(; begin != end; ++begin){
		this->add_field(*begin);
	}
}

void tree::set_operator(operators o){
	this->_operator = o;
}

void tree::set_relation(string r){
	this->_relation = r;
}

bool tree::has_relation(string r){
	if(this->_operator == NONE && this->_relation == r)
		return true;
	if(this->_right && this->_right->has_relation(r))
		return true;
	if(this->_left && this->_left->has_relation(r))
		return true;
	return false;
}

void tree::clear_fields(){
	this->_fields.clear();
}

tree& tree::operator=(const tree& t){
	delete this->_left;
	this->_left = NULL;
	delete this->_right;
	this->_right = NULL;
	this->_fields.clear();
	this->relations_joined.clear();

	this->cost_so_far = t.cost_so_far;
	list<string>::const_iterator it;
	for(it = t.relations_joined.begin(); it != t.relations_joined.end(); ++it){
		this->relations_joined.push_back(*it);
	}
	this->join_used = t.join_used;
	this->selection_used = t.selection_used;
	this->num_tuples = t.num_tuples; // result cardinality
	this->tuple_size = t.tuple_size; // new tuple size, sum in case of join

	this->set_operator(t.get_operator());
	string r = t.get_relation();
	this->set_relation(r);
	list<string>::const_iterator init, end;
	t.get_fields(init, end);
	this->set_fields(init, end);
	if(t._left){
		this->_left = new tree();
		this->copy(this->_left, t._left);
	}
	if(t._right){
		this->_right = new tree();
		this->copy(this->_right, t._right);
	}
	return *this;
}

void tree::copy(tree* td, const tree* ts){
	if (ts == NULL){
		return;
	}

	td->cost_so_far = ts->cost_so_far;
	list<string>::const_iterator it;
	td->relations_joined.clear();
	for(it = ts->relations_joined.begin(); it != ts->relations_joined.end(); ++it){
		td->relations_joined.push_back(*it);
	}
	td->join_used = ts->join_used;
	td->selection_used = ts->selection_used;
	td->num_tuples = ts->num_tuples; // result cardinality
	td->tuple_size = ts->tuple_size; // new tuple size, sum in case of join

	td->set_operator(ts->get_operator());
	td->set_relation(ts->get_relation());
	list<string>::const_iterator init, end;
	ts->get_fields(init, end);
	td->set_fields(init, end);
	if(ts->_left){
		td->_left = new tree();
		copy(td->_left, ts->_left);
	}
	if(ts->_right){
		td->_right = new tree;
		copy(td->_right, ts->_right);
	}
}

void tree::set_tree(tree& t, tree_child c){
	tree *tn = new tree();
	this->copy(tn, &t);
	switch(c){
		case CHILD_LEFT:
			this->_left = tn;
			break;
		case CHILD_RIGHT:
			this->_right = tn;
			break;
	}
}

void print_fields(ostream& out, list<string>& fields){
	for(list<string>::iterator it = fields.begin(); it != fields.end(); ++it){
		if(it != fields.begin())
			out << ", ";
		out << *it;
	}
}

void tree::set(tree *t, tree_child c){
	switch(c){
		case CHILD_LEFT:
			this->_left = t;
			break;
		case CHILD_RIGHT:
			this->_right = t;
			break;
	}
}

ostream& tree::print(ostream& out){
	switch(this->_operator){
		case SELECTION:
			out << "σ{cost:" << this->cost_so_far << ", " << this->selection_used <<"}[";
			print_fields(out, this->_fields);
			out << "](";
				if(this->_left)
					this->_left->print(out);
			out << ")";
			return out;
		case PROJECTION:
			out << "π{cost:" << this->cost_so_far << ", " << this->join_used <<"}[";
			print_fields(out, this->_fields);
			out << "](";
				if(this->_left)
					this->_left->print(out);
			out << ")";
			return out;
		case JOIN:
			out << "(";
			if(this->_left){
				this->_left->print(out);
				//out << ")";
			}
			out << "⋈{cost:" << this->cost_so_far << ", " << this->join_used <<"}[";
			print_fields(out, this->_fields);
			out << "]";
			if(this->_right){
				//out << "(";
				this->_right->print(out);
			}
			out << ")";
			return out;
		case NONE:
			return out << this->_relation;
	}
	return out;
}

/*
	Calculates full tree cost, this is NOT for systemr
*/
int get_cost(catalog& c){
	return 0;
}

