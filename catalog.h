#ifndef CATALOG_H
#define CATALOG_H
#include <list>
#include <map>
#include <iostream>
#include <string>
#include "index.h"
#include "query.h"

using namespace std;

class tableinfo{
	public:
		string name;
		int tuples_number;
		int tuples_size;
		int tuples_per_page;
		int num_pages;
		list<index> indexes;
		map<string, string> joinables;

		tableinfo& operator=(const tableinfo&);
};

class btree;

class catalog{
	public:
		catalog();
		index& get_index_by_column(tableinfo &, string, bool &);
		void set_page_size(int);
		int get_page_size();
		void set_cache_size(int);
		int get_cache_size();
		void add_table(string name, int tuples_number, int tuples_size, int tuples_per_page, int num_pages);
		void add_index(string, index&);
		void add_joinable(string, string, string);
		bool are_joinable(string, string, string &);
		bool has_table(string name);
		tableinfo& get_tableinfo(string);
		void get_tableinfo(string, tableinfo&);
		bool has_ordered_index(string table, string idx, string field);
		ostream& print(ostream&);
		// Cost calculation functions
		static unsigned long long calculate_cost(tableinfo &info, index &ind, double fr, bool field_match);

		static double calculate_join_fr(catalog &c, btree &l, btree &r, const string &column);

		static double calculate_fr(catalog &c, tableinfo &info, TABLE_OPERATION &to, index &ind);

		static unsigned long long calculate_scan_cost(tableinfo &info);

		static void nl_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static void bnl_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static void hj_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static void sm_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static void ij_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, query &q, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static unsigned long long calculate_bnl_join_cost(btree& l, btree &r, string column, query &q, catalog &c, string& join_used, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static unsigned long long calculate_best_join_cost(btree& l, btree &r, string column, query &q, catalog &c, string& join_used, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static unsigned long long calculate_join_cost(btree& t, string column, query &q, catalog &c, string& join_used, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr);

		static unsigned long long materialization_cost(btree&, catalog &);

		static string get_random_join(btree &);

	private:
		map<string, tableinfo> _tables;
		int _page_size;
		int _cache_size;

};

inline ostream& operator << (ostream& os, catalog &c){
	return c.print(os);
}

inline ostream& operator << (ostream &os, list<index> &indexes){
	for(list<index>::iterator it = indexes.begin(); it != indexes.end(); ++it){
		os << *it << endl;
	}
	return os;
}

inline ostream& operator << (ostream& os, tableinfo &t){
	return os << "table " << t.name << ": " << endl
			<< "\ttuples number: " << t.tuples_number << endl
			<< "\ttuples size: " << t.tuples_size << endl
			<< "\ttuples per page: " << t.tuples_per_page << endl
			<< "\tnumber of pages: " << t.num_pages << endl
			<< "indexes:" << endl << t.indexes;
}


#endif
