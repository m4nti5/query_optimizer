#include <cassert>
#include <cstdlib>
#include <cmath>
#include "catalog.h"
#include "btree.h"

tableinfo& tableinfo::operator=(const tableinfo& t){
	this->name = t.name;
	this->tuples_number = t.tuples_number;
	this->tuples_size = t.tuples_size;
	this->tuples_per_page = t.tuples_per_page;
	this->num_pages = t.num_pages;
	this->indexes.clear();
	for(list<index>::const_iterator it = t.indexes.begin(); it != t.indexes.end(); ++it){
		this->indexes.push_back(*it);
	}
	return *this;
}

catalog::catalog(){

}

index& catalog::get_index_by_column(tableinfo &t, string c, bool &has_ind){
	list<index>::iterator it;
	has_ind = false;
	for(it = t.indexes.begin(); it != t.indexes.end(); ++it){
		list<string>::const_iterator begin, end;
		it->get_fields(begin, end);
		if(*begin == c){
			has_ind = true;
			return *it;
		}
	}
	return *it;
}

void catalog::set_page_size(int ps){
	this->_page_size = ps;
}

int catalog::get_page_size(){
	return this->_page_size;
}

void catalog::set_cache_size(int cs){
	this->_cache_size = cs;
}

int catalog::get_cache_size(){
	return this->_cache_size;
}

void catalog::add_table(string name, int tn, int ts, int tpp, int np){
	tableinfo t;
	t.name = name;
	t.tuples_number = tn;
	t.tuples_size = ts;
	t.tuples_per_page = tpp;
	t.num_pages = np;
	this->_tables[name] = t;
}

void catalog::add_index(string name, index& i){
	assert(this->_tables.find(name) != this->_tables.end());
	this->_tables[name].indexes.push_back(i);
}

void catalog::add_joinable(string relation1, string relation2, string column){
	assert(this->_tables.find(relation1) != this->_tables.end());
	assert(this->_tables.find(relation2) != this->_tables.end());
	this->_tables[relation1].joinables[relation2] = column;
	this->_tables[relation2].joinables[relation1] = column;
}

bool catalog::are_joinable(string r1, string r2, string &column){
	assert(this->_tables.find(r1) != this->_tables.end());
	if(this->_tables[r1].joinables.find(r2) != this->_tables[r1].joinables.end()){
		column = this->_tables[r1].joinables[r2];
		return true;
	}
	return false;
}

bool catalog::has_table(string name){
	if(this->_tables.find(name) == this->_tables.end())
		return false;
	return true;
}

tableinfo& catalog::get_tableinfo(string name){
	return (*this->_tables.find(name)).second;
}

void catalog::get_tableinfo(string table, tableinfo& t){
	t = (*(this->_tables.find(table))).second;
}

bool catalog::has_ordered_index(string table, string idx, string field){
	if(this->has_table(table)){
		tableinfo &t = get_tableinfo(table);
		list<index>::iterator it;
		for(it = t.indexes.begin(); it != t.indexes.end(); ++it){
			if((*it).id == idx){
				list<string>::const_iterator begin, end;
				it->get_fields(begin, end);
				for(; begin != end; ++begin){
					if(*begin == field)
						return true;
				}
			}
		}
	}
	return false;
}

ostream& catalog::print(ostream& os){
	os << "page size: " << this->_page_size << endl;
	for(map<string, tableinfo>::iterator it = this->_tables.begin(); it != this->_tables.end(); ++it){
		//os << (*it).second;
		tableinfo &t = get_tableinfo((*it).first);
		os << t;
	}
	return os;
}

unsigned long long catalog::calculate_scan_cost(tableinfo &info){
	return info.num_pages;
}

int max_value(int v1, int v2){
	return v1 > v2 ? v1 : v2;
}

int to_int(string t){
	return atoi(t.c_str());
}

double catalog::calculate_join_fr(catalog &c, btree &l, btree &r, const string &column){
	double fr = 0.1;
	string r1, r2;
	if((l.is_node() && !l.is_selection()) || (r.is_node() && !r.is_selection()))
		return fr;
	fr = 1.0;
	if(l.is_selection()){
		fr *= l.fr;
		r1 = l.get_selection_relation();
	}else{
		r1 = l.get_relation();
	}
	if(r.is_selection()){
		fr *= r.fr;
		r2 = r.get_selection_relation();
	}else{
		r2 = r.get_relation();
	}
	tableinfo &info_r1 = c.get_tableinfo(r1);
	tableinfo &info_r2 = c.get_tableinfo(r2);
	for(list<index>::iterator it = info_r1.indexes.begin(); it != info_r1.indexes.end(); ++it){
		if(it->get_first_field() == column){
			for(list<index>::iterator it_r2 = info_r2.indexes.begin(); it_r2 != info_r2.indexes.end(); ++it_r2){
				if(it_r2->get_first_field() == column){
					return fr * 1/max_value(it->get_number_keys(),it_r2->get_number_keys());
				}
			}
		}
	}
	fr *= 0.1;
	return fr;
}

double catalog::calculate_fr(catalog &c, tableinfo &info, TABLE_OPERATION &to, index &ind){
	double fr = 0.1;
	int value;
	switch(to.op){
		case EQUAL:
			fr = (double)1/ind.get_number_keys();
			break;
		case MINOR:
			value = to_int(to.v.value);
			if(value < ind.get_max_value() && value > ind.get_min_value()){
				fr = (double)(value - ind.get_min_value()) / (ind.get_max_value() - ind.get_min_value());
			}
			break;
		case MAYOR:
			value = to_int(to.v.value);
			if(value < ind.get_max_value() && value > ind.get_min_value()){
				fr = (double)(ind.get_max_value() - value) / (ind.get_max_value() - ind.get_min_value());
			}
			break;
	}
	return fr;
}

unsigned long long catalog::calculate_cost(tableinfo &info, index &ind, double fr, bool field_match){
	switch(ind.get_type()){
		case HASH:
			if(field_match){
				return 2;
			}
		case BTREE:
			if(ind.get_order() == CLUSTERED){
				if(field_match)
					return  1 + fr * (ind.get_num_pages() + info.num_pages);
				else
					return ind.get_num_pages() + info.num_pages;
			}else{
				if(field_match)
					return 1 + fr * (ind.get_num_pages() + info.tuples_number);
				else
					return (ind.get_num_pages() + info.tuples_number);
			}
			break;

	}
	return info.num_pages;
}

/**
* Calculates the nested loop cost
*/
void catalog::nl_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	unsigned long long nested_loop_1 = M + r.num_tuples * M * N;
	unsigned long long nested_loop_2 = N + l.num_tuples * M * N;
	bool chosen = false;
	if(nested_loop_1 < cost){
		cost = nested_loop_1;
		chosen = true;
	}
	if(nested_loop_2 < cost){
		cost = nested_loop_2;
		chosen = true;
	}

	if(chosen){
		fr = calculate_join_fr(c, l, r, column);
		join_chosen = "nl";
		num_tuples = (unsigned long long)(double)(fr * l.num_tuples * r.num_tuples);
		tuple_size = r.tuple_size + l.tuple_size;
	}
}

/**
* Calculates the block nested loop join cost
*/
void catalog::bnl_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	// BLOCK NESTED LOOP
	bool replaced = false;
	unsigned long long block_nested_loop_1 = M + (1 + (double)M/(c.get_cache_size()-2))*N;
	if(block_nested_loop_1 < cost){
		cost = block_nested_loop_1;
		join_chosen = "bnl";
		replaced = true;
	}

	unsigned long long block_nested_loop_2 = N + (1 + (double)N/(c.get_cache_size()-2))*M;
	if(block_nested_loop_2 < cost){
		cost = block_nested_loop_2;
		join_chosen = "bnl";
		replaced = true;
	}
	if(replaced){
		fr = calculate_join_fr(c, l, r, column);
		num_tuples = (unsigned long long)(double)(fr * l.num_tuples * r.num_tuples);
		tuple_size = r.tuple_size + l.tuple_size;
	}
}

/**
* Calculates the hash join cost
*/
void catalog::hj_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	bool chosen = false;

	unsigned long long hj_cost = 3*(M + N);
	if(hj_cost < cost){
		cost = hj_cost;
		chosen = true;
	}

	if(chosen){
		fr = calculate_join_fr(c, l, r, column);
		join_chosen = "hj";
		num_tuples = (unsigned long long)(double)(fr * l.num_tuples * r.num_tuples);
		tuple_size = r.tuple_size + l.tuple_size;
	}
}

/**
* Calculates the sort merge join cost
*/
void catalog::sm_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	bool chosen = false;

	unsigned long long cost_m = (unsigned long long)((double)M*log(M));
	unsigned long long cost_n = (unsigned long long)((double)N*log(N));
	if(r.is_ordered(column, c)){
		cost_n = 0;
	}
	unsigned long long sm_cost = cost_m + cost_n + M + N;
	if(sm_cost < cost){
		cost = sm_cost;
		join_chosen = "sm";
		chosen = true;
	}

	if(chosen){
		fr = calculate_join_fr(c, l, r, column);
		join_chosen = "sm";
		num_tuples = (unsigned long long)(double)(fr * l.num_tuples * r.num_tuples);
		tuple_size = r.tuple_size + l.tuple_size;
	}
}

/**
* Calculates the index join cost
*/
void catalog::ij_join_cost(btree &l, btree &r, unsigned long long &cost, unsigned long long M, unsigned long long N, string column, query &q, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	fr = 0.1;
	// if not selection defined, check if an index join can be made
	if(!r.is_node()){
		tableinfo &info = c.get_tableinfo(r.get_relation());
		bool has_index = false;
		index &ind = c.get_index_by_column(info, column, has_index);
		if(has_index){
			fr = calculate_join_fr(c, l, r, column);
			double ind_c = 1.0;
			switch(ind.get_type()){
				case HASH:
					ind_c = 1.2;
					break;
				case BTREE:
					ind_c = 4.0;
					break;
			}
			switch(ind.get_order()){
				case UNCLUSTERED:
					ind_c *= info.tuples_number;
					break;
				default:
					break;
			}
			unsigned long long inl_cost = M + (unsigned long long)((double)fr * M * ind_c);
			if(inl_cost < cost){
				cost = inl_cost;
				join_chosen = "ij";
				num_tuples = (unsigned long long)(double)(fr * l.num_tuples * r.num_tuples);
				tuple_size = r.tuple_size + l.tuple_size;
			}
		}
	}
}

/**
* Selects bnl for a join
*/
unsigned long long catalog::calculate_bnl_join_cost(btree &l, btree &r, string column, query &q, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	unsigned long long cost;
	double tuple_per_page_rate = (double)r.tuple_size / c.get_page_size();
	unsigned long long N = r.num_tuples * tuple_per_page_rate;
	if(N == 0)
		N = 1;
	tuple_per_page_rate = (double)l.tuple_size / c.get_page_size();
	unsigned long long M = l.num_tuples * tuple_per_page_rate;
	if(M == 0)
		M = 1;
	fr = calculate_join_fr(c, l, r, column);
	cost = 0xFFFFFFFFFFFFFFFFull;
	bnl_join_cost(l, r, cost, M, N, column, c, join_chosen, num_tuples, tuple_size, fr);
	return cost;
}

/**
* Checks for all posible join operations and selects the cheapest based in the tree characteristics
* and number of elements
*/
unsigned long long catalog::calculate_best_join_cost(btree &l, btree &r, string column, query &q, catalog &c, string& join_chosen, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	unsigned long long cost = 0xFFFFFFFFFFFFFFFFull;
	double tuple_per_page_rate = (double)r.tuple_size / c.get_page_size();
	unsigned long long N = r.num_tuples * tuple_per_page_rate;
	if(N == 0)
		N = 1;
	tuple_per_page_rate = (double)l.tuple_size / c.get_page_size();
	unsigned long long M = l.num_tuples * tuple_per_page_rate;
	if(M == 0)
		M = 1;
	// NESTED LOOP JOIN
	nl_join_cost(l, r, cost, M, N, column, c, join_chosen, num_tuples, tuple_size, fr);

	// BLOCK NESTED LOOP
	bnl_join_cost(l, r, cost, M, N, column, c, join_chosen, num_tuples, tuple_size, fr);

	// HASH JOIN
	hj_join_cost(l, r, cost, M, N, column, c, join_chosen, num_tuples, tuple_size, fr);

	// SORT MERGE
	sm_join_cost(l, r, cost, M, N, column, c, join_chosen, num_tuples, tuple_size, fr);

	// INDEX JOIN
	ij_join_cost(l, r, cost, M, N, column, q, c, join_chosen, num_tuples, tuple_size, fr);

	return cost;
}

/**
* Calculates joins cost, doesnt change anything
*/
unsigned long long catalog::calculate_join_cost(btree& t, string column, query &q, catalog &c, string& join_used, unsigned long long& num_tuples, unsigned long long& tuple_size, double& fr){
	unsigned long long cost;
	if(!t.is_node())
		return 0;
	btree &l = t.get_son(CHILD_LEFT); 
	btree &r = t.get_son(CHILD_RIGHT);
	double tuple_per_page_rate = (double)r.tuple_size / c.get_page_size();
	unsigned long long N = r.num_tuples * tuple_per_page_rate;
	if(N == 0)
		N = 1;
	tuple_per_page_rate = (double)l.tuple_size / c.get_page_size();
	unsigned long long M = l.num_tuples * tuple_per_page_rate;
	if(M == 0)
		M = 1;
	fr = 0.1;
	cost = 0xFFFFFFFFFFFFFFFFull;
	bool is_index_joinable = false;
	if(t.son_is_relation(CHILD_RIGHT)){
		is_index_joinable = true;
	}
	if(t.join_used == "nl"){
		nl_join_cost(l, r, cost, M, N, column, c, join_used, num_tuples, tuple_size, fr);
	}else if(t.join_used == "hj"){
		hj_join_cost(l, r, cost, M, N, column, c, join_used, num_tuples, tuple_size, fr);
	}else if(t.join_used == "sm"){
		sm_join_cost(l, r, cost, M, N, column, c, join_used, num_tuples, tuple_size, fr);
	}else if(t.join_used == "ij" && is_index_joinable){
		ij_join_cost(l, r, cost, M, N, column, q, c, join_used, num_tuples, tuple_size, fr);
	}else{
		bnl_join_cost(l, r, cost, M, N, column, c, join_used, num_tuples, tuple_size, fr);
	}
	return cost;
}

unsigned long long catalog::materialization_cost(btree& tree, catalog &c){
	double tuple_per_page_rate = tree.tuple_size / c.get_page_size();
	unsigned long long materialization = tree.num_tuples * tuple_per_page_rate;
	if(materialization <= 0)
		materialization = 1;
	return materialization;
}

string catalog::get_random_join(btree &tree){
	vector<string> joins;
	joins.push_back("nl");
	joins.push_back("hj");
	joins.push_back("sm");
	/*
	if(tree.son_is_relation(CHILD_RIGHT)){
		tableinfo &info = c.get_tableinfo(tree.get_son(CHILD_RIGHT));
		bool has_index = false;
		index &ind = c.get_index_by_column(info, tree., has_index);
		if(has_index)
			joins.push_back("ij");
	}
	*/
	return joins[rand() % joins.size()];
}
