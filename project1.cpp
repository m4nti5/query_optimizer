#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <deque>
#include <cstdio>
#include <cstdlib>
#include "btree.h"
#include "catalog.h"
#include "index.h"
#include "query.h"
#include "systemr.h"
#include "iterative_improvement.h"

using namespace std;

void parse_query(query& q, catalog &c){
	int N;
	cin >> N;
	for(int i = 0; i < N; ++i){
		string pf;
		cin >> pf;
		q.add_projection_field(pf);
	}
	cin >> N;
	for(int i = 0; i < N; ++i){
		string r;
		cin >> r;
		q.add_relation(r);
	}
	cin >> N;
	for(int i = 0; i < N; ++i){
		string table, field, type, value, op, table2;
		cin >> table >> field >> op >> type;
		VALUE_TYPE vt;
		if(type == "c"){
			vt.type = CONSTANT;
		}else{
			vt.type = COLUMN;
			cin >> vt.table;
		}
		cin >> vt.value;
		COMPARISION cmp;
		if(op == "<"){
			cmp = MINOR;
		}else if(op == "="){
			cmp = EQUAL;
		}else{
			cmp = MAYOR;
		}
		q.add_operation(table, cmp, field, vt);
		if(vt.type == COLUMN){
			VALUE_TYPE vt2;
			vt2.type = COLUMN;
			vt2.table = table;
			vt2.value = field;
			q.add_operation(vt.table, cmp, vt.value, vt2);
			c.add_joinable(table, vt.table, field);
		}
	}
	cout << q << endl;
}

void parse_catalog(catalog& c){
	void add_index(string, index&);
	int N, M, NFIELDS, page_size, cache_size;
	cin >> page_size >> cache_size >> N;
	c.set_page_size(page_size);
	c.set_cache_size(cache_size);
	for(int i = 0; i < N; ++i){
		string name;
		int tuples_number, tuples_size, tuples_per_page, num_pages;
		cin >> name >> tuples_number >> tuples_size >> tuples_per_page >> num_pages;
		c.add_table(name, tuples_number, tuples_size, tuples_per_page, num_pages);
		cin >> M;
		for(int j = 0; j < M; ++j){
			index ind;
			string id, type, order;
			cin >> id >> type >> order >> NFIELDS;
			index_type itp;
			if(type == "HASH"){
				itp = HASH;
			}else{
				itp = BTREE;
			}
			index_order ior;
			if(order == "CLUSTERED"){
				ior = CLUSTERED;
			}else{
				ior = UNCLUSTERED;
			}
			ind.id = id;
			ind.set_type(itp);
			ind.set_order(ior);
			for(int k = 0; k < NFIELDS; ++k){
				string field;
				cin >> field;
				ind.add_field(field);
			}
			int nkeys, max_val, min_val, num_pages, tpp;
			cin >> nkeys >> max_val >> min_val >> num_pages >> tpp;
			ind.set_number_keys(nkeys);
			ind.set_max_value(max_val);
			ind.set_min_value(min_val);
			ind.set_num_pages(num_pages);
			ind.set_tuples_per_pages(tpp);
			c.add_index(name, ind);
		}
	}
	cout << c << endl;
}

int main(){
	int seed = time(NULL);
    srand(seed);
	catalog c;
	query q;
	btree plan;
	unsigned long long cost = 0;
	// Initialization

	// Read catalog
	parse_catalog(c);

	// Read query
	parse_query(q, c);

	// Calculate best plan

	systemr(c, q, plan, cost);

	// print cost and plan
	cout << "Systemr best plan: " << plan <<  ", cost: " << cost << endl;
	return 0;
}
