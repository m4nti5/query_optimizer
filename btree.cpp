#include "btree.h"
#include <deque>
#include <vector>
#include <sstream>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include "query.h"

void btree::node::print_fields(ostream &out, list<string> &fields){
	list<string>::iterator it;
	bool first = true;
	for(it = fields.begin(); it != fields.end(); ++it){
		if(first)
			first = false;
		else
			out << ", ";
		out << *it;
	}
}

ostream& btree::node::print(ostream &out, btree& t){
	switch(this->op){
		case SELECTION:
			out << "σ";
			if(PRINT_VERBOSE){
				out <<"{" << "cost: " << t.cost_so_far << ", " << t.selection_used << ", tuples: " << t.num_tuples << "}[";
					print_fields(out, this->fields);
				out << "]";
			}
			out << "(";
				if(this->left) this->left->print(out);
			out << ")";
			break;
		case JOIN:
			out << "(";
			if(this->left){
				this->left->print(out);
			}
			out << "⋈";
			if(PRINT_VERBOSE){
				out << "{" << "cost: " << t.cost_so_far << ", " << t.join_used << ", tuples: " << t.num_tuples << "}[";
					print_fields(out, this->fields);
				out << "]";
			}
				if(this->right) this->right->print(out);
			out << ")";
			break;
		default:
			break;
	}
	return out;
}

btree::node::node(operators op){
	this->op = op;
	this->left = NULL;
	this->right = NULL;
}

btree::node::node(){
	this->op = SELECTION;
	this->left = NULL;
	this->right = NULL;
}

btree::leaf::leaf(){

}

btree::leaf::leaf(string rel){
	this->relation = rel;
}

btree::btree(){
	this->cost_so_far = 0;
	this->num_tuples = 0; // result cardinality
	this->tuple_size = 0; // new tuple size, sum in case of join
	this->fr = 0;

	this->_node = NULL;
	this->_leaf = NULL;
}

void btree::copy_systemr_info(const btree &t){
	this->cost_so_far = t.cost_so_far;
	this->join_used = t.join_used;
	this->selection_used = t.selection_used;
	this->num_tuples = t.num_tuples; // result cardinality
	this->tuple_size = t.tuple_size; // new tuple size, sum in case of join
	this->fr = t.fr;
	this->relations_joined.clear();
	for(list<string>::const_iterator it = t.relations_joined.begin(); it != t.relations_joined.end(); ++it){
		this->relations_joined.push_back(*it);
	}
}

btree::btree(const btree& src){
	this->copy_systemr_info(src);
	copy(src._node, this->_node);
	copy(src._leaf, this->_leaf);
}

btree::~btree(){
	if(this->_node)
		free(this->_node);
	if(this->_leaf)
		free(this->_leaf);
}

void btree::get_all_nodes(map<int, btree*>& nodes){
	if(!this->is_node() || this->_node->op == SELECTION)
		return;
	nodes[nodes.size()] = this;
	this->_node->left->get_all_nodes(nodes);
	this->_node->right->get_all_nodes(nodes);
}

void btree::get_random_node(btree *& res){
	res = NULL;
	if(!this->is_node() || this->_node->op == SELECTION)
		return;
	map<int, btree*> nodes;
	nodes[0] = this;
	this->_node->left->get_all_nodes(nodes);
	this->_node->right->get_all_nodes(nodes);
	int choice = rand() % nodes.size();
	res = nodes[choice];
}

/**
* Checks if this tree has a left join
*/
bool btree::is_left_join(){
	if(this->is_node() && this->_node->left->is_node() && this->_node->left->_node->op == JOIN)
		return true;
	return false;
}

/**
* Checks if this tree has a right join
*/
bool btree::is_right_join(){
	if(this->is_node() && this->_node->right->is_node() && this->_node->right->_node->op == JOIN)
		return true;
	return false;
}

/**
* R join S -> S join R
*/
bool btree::conmutate(){
	if(!this->is_node() || this->_node->op != JOIN)
		return false;

	btree *nswap = this->_node->left;
	this->_node->left = this->_node->right;
	this->_node->right = nswap;
	return true;
}

/**
* (R join S) join T -> R join (S join T)
*/
bool btree::associative(){
	if(!this->is_left_join()){
		return false;
	}
	btree *parent = this->_node->left;
	btree *R = this->_node->left->_node->left;
	btree *S = this->_node->left->_node->right;
	btree *T = this->_node->right;
	this->_node->right = parent;
	this->_node->left = R;
	parent->_node->left = S;
	parent->_node->right = T;
	return true;
}

/**
* R join (S join T) -> (R join S) join T
*/
bool btree::backward_associative(){
	if(!this->is_right_join()){
		return false;
	}
	btree *parent = this->_node->right;
	btree *R = this->_node->left;
	btree *S = this->_node->right->_node->left;
	btree *T = this->_node->right->_node->right;
	this->_node->left = parent;
	this->_node->right = T;
	parent->_node->left = R;
	parent->_node->right = S;
	return true;
}

/**
* (R join S) join T -> (R join T) join S
*/
bool btree::left_join(){
	if(!this->is_left_join())
		return false;

	btree *parent = this->_node->left;
	btree *S = this->_node->left->_node->right;
	btree *T = this->_node->right;
	parent->_node->right = T;
	this->_node->right = S;
	return true;
}

/**
* R join (S join T) -> S join (R join T)
*/
bool btree::right_join(){
	if(!this->is_right_join())
		return false;

	btree *parent = this->_node->right;
	btree *R = this->_node->left;
	btree *S = this->_node->right->_node->left;
	parent->_node->left = R;
	this->_node->left = S;
	return true;
}

/**
* Randomly chooses a subtree and applies conmutative
* property
*/
bool btree::random_conmutate(){
	if(!this->is_node() || this->_node->op != JOIN)
		return false;
	btree *rtree = NULL;
	do{
		this->get_random_node(rtree);
	}while(rtree == NULL || !rtree->is_node() || rtree->_node->op != JOIN);

	//cout << endl << "Conmutate Chosen: " << *rtree << endl << endl;

	return rtree->conmutate();
}

/**
* Checks if the tree has a left join
*/
bool btree::has_left_join(){
	if(!this->is_node() || this->_node->op != JOIN)
		return false;
	if(this->is_left_join()){
		return true;
	}

	return (this->_node->left->has_left_join() || this->_node->right->has_left_join());
}

/**
* Checks if the tree has a right join
*/
bool btree::has_right_join(){
	if(!this->is_node() || this->_node->op != JOIN)
		return false;
	if(this->is_right_join()){
		return true;
	}

	return (this->_node->left->has_right_join() || this->_node->right->has_right_join());
}

/**
* Randomly choses a left join subtree and applies associative
* operation over it
*/
bool btree::random_left_associative(){
	if(!this->is_node() || this->_node->op != JOIN || !this->has_left_join())
		return false;
	btree *rtree = NULL;
	do{
		this->get_random_node(rtree);
	}while(rtree == NULL || !rtree->is_left_join());

	//cout << endl << "Associative Chosen: " << *rtree << endl << endl;

	return rtree->associative();
}

/**
* Randomly choses a right join subtree and applies associative
* operation over it
*/
bool btree::random_right_associative(){
	if(!this->is_node() || this->_node->op != JOIN || !this->has_right_join())
		return false;
	btree *rtree = NULL;
	do{
		this->get_random_node(rtree);
	}while(rtree == NULL || !rtree->is_right_join());

	//cout << endl << "Associative Chosen: " << *rtree << endl << endl;

	return rtree->backward_associative();
}

bool btree::random_associative(){
	int type = rand() % 2;
	if(type == 0){
		return this->random_left_associative();
	}
	return this->random_right_associative();
}

/**
* Randomly choses a left join subtree and applies left join
* operation over it
*/
bool btree::random_left_join(){
	if(!this->is_node() || this->_node->op != JOIN || !this->has_left_join())
		return false;
	btree *rtree = NULL;
	do{
		this->get_random_node(rtree);
	}while(rtree == NULL || !rtree->is_left_join());

	//cout << endl << "Left join Chosen: " << *rtree << endl << endl;

	return rtree->left_join();
}


void btree::set_random_join(){
	this->join_used = catalog::get_random_join(*this);
}

/**
* Randomly choses a left join subtree and applies associative
* operation over it
*/
bool btree::random_right_join(){
	if(!this->is_node() || this->_node->op != JOIN || !this->has_right_join())
		return false;
	btree *rtree = NULL;
	do{
		this->get_random_node(rtree);
	}while(rtree == NULL || !rtree->is_right_join());

	//cout << endl << "Right join Chosen: " << *rtree << endl << endl;

	return rtree->right_join();
}

bool btree::is_ordered(string f, catalog &cat){
	if(this->_node && this->_node->left && this->_node->op == SELECTION){
		string s1, index;
		stringstream ss;
		ss << this->selection_used;
		ss >> s1 >> index;
		return cat.has_ordered_index(this->_node->left->get_relation(), index, f);
	}
	return false;
}

void btree::get_fields(list<string>::const_iterator& init, list<string>::const_iterator& end) const{
	if(this->_node == NULL)
		return;
	init = this->_node->fields.begin();
	end = this->_node->fields.end();
}

operators btree::get_operator() const{
	if(this->_node == NULL)
		return NONE;
	return this->_node->op;
}

string btree::get_relation() const{
	if(this->_node != NULL)
		return "";
	return this->_leaf->relation;
}

string btree::get_selection_relation(){
	if(!this->is_selection())
		return "";
	return this->_node->left->get_relation();
}

void btree::get_relations(list<string> &relations){
	if(this->_node != NULL){
		if(this->_node->left)
			this->_node->left->get_relations(relations);
		if(this->_node->right)
			this->_node->right->get_relations(relations);
	}
	if(this->_leaf != NULL){
		relations.push_back(this->_leaf->relation);
	}
}

string btree::get_selection_relation() const{
	return this->get_relation();
}

bool btree::is_node(){
	if(this->_node == NULL)
		return false;
	return true;
}

void btree::add_field(string f){
	if(this->_leaf != NULL)
		return;
	if(this->_node == NULL)
		this->_node = new node();
	this->_node->fields.push_back(f);
}

void btree::clear_fields(){
	if(this->_node == NULL)
		return;
	this->_node->fields.clear();
}

void btree::set_fields(list<string>::const_iterator& init, list<string>::const_iterator& end){
	if(this->_node == NULL){
		this->_node = new node();
	}
	this->_node->fields.clear();
	for(; init != end; ++init){
		this->_node->fields.push_back(*init);
	}
}

void btree::set_operator(operators op){
	if(this->_node == NULL){
		this->_node = new node(op);
	}
}

void btree::set_relation(string rel){
	if(this->_leaf == NULL){
		this->_leaf = new leaf(rel);
	}
}

void btree::set_tree(btree &tree, tree_child son){
	if(this->_node == NULL){
		this->_node = new node();
	}
	switch(son){
		case CHILD_RIGHT:
			copy(&tree, this->_node->right);
			break;
		case CHILD_LEFT:
			copy(&tree, this->_node->left);
			break;
	}
}

void btree::set(btree *tree, tree_child son){
	if(this->_node == NULL){
		this->_node = new node();
	}
	switch(son){
		case CHILD_RIGHT:
			this->_node->right = tree;
			break;
		case CHILD_LEFT:
			this->_node->left = tree;
			break;
	}
}

bool btree::has_relation(string r){
	bool res = false;
	if(this->_node != NULL){
		if(this->_node->left != NULL)
			res = this->_node->left->has_relation(r);
		if(res)
			return true;
		if(this->_node->right != NULL)
			res = this->_node->right->has_relation(r);
		if(res)
			return true;
	}
	if(this->_leaf != NULL){
		res = (this->_leaf->relation == r);
	}
	return res;
}

ostream& btree::print(ostream& out){
	if(this->_node != NULL){
		return this->_node->print(out, *this);
	}
	if(this->_leaf != NULL){
		return out << this->_leaf->relation;
	}
	return out;
}

void btree::copy(node* src, node*& dst){
	if (src == NULL){
		dst = NULL;
	}else{
		dst = new node();
		dst->op = src->op;
		list<string>::iterator init;
		dst->fields.clear();
		for(init = src->fields.begin(); init != src->fields.end(); init++){
			dst->fields.push_back(*init);
		}

		copy(src->left, dst->left);
		copy(src->right, dst->right);
	}
}

void btree::copy(btree *src, btree*& dst){
	if(src == NULL){
		dst = NULL;
	}else{
		dst = new btree();
		dst->copy_systemr_info(*src);
		copy(src->_node, dst->_node);
		copy(src->_leaf, dst->_leaf);
	}

}

void btree::copy(leaf *src, leaf*& dst){
	if(src == NULL){
		dst = NULL;
	}else{
		dst = new leaf(src->relation);
	}
}

void btree::free(node*& ts){
	if (ts != NULL){
		free(ts->left);
		free(ts->right);
		delete ts;
		ts = NULL;
	}
}

void btree::free(leaf*& ts){
	if (ts != NULL){
		delete ts;
		ts = NULL;
	}
}

void btree::free(btree*& ts){
	if(ts != NULL){
		free(ts->_node);
		free(ts->_leaf);
		delete ts;
		ts = NULL;
	}
}

btree& btree::operator=(const btree& bt){
	if(this == &bt)
		return *this;
	this->copy_systemr_info(bt);
	free(this->_node);
	this->_node = NULL;
	copy(bt._node, this->_node);

	free(this->_leaf);
	this->_leaf = NULL;
	copy(bt._leaf, this->_leaf);
	return *this;
}

bool btree::calculate_cost(query &q, catalog &c){
	if(this->_node != NULL){
		if(this->_node->op != JOIN)
			return false;
		unsigned long long cost_l = 0, cost_r = 0, cost = 0, materialization;
		assert(this->_node->left != NULL);
		if(!this->_node->left->calculate_cost(q, c))
			return false;
		cost_l = this->_node->left->cost_so_far;
		assert(this->_node->right != NULL);
		if(!this->_node->right->calculate_cost(q, c))
			return false;
		cost_r = this->_node->right->cost_so_far;
		list<string> relations_r;
		list<string> relations_l;
		bool joinable = false;
		this->_node->right->get_relations(relations_r);
		this->_node->left->get_relations(relations_l);
		string column, l_relation, r_relation;
		for(list<string>::iterator itl = relations_l.begin(); itl != relations_l.end() && !joinable; ++itl){
			for(list<string>::iterator itr = relations_r.begin(); itr != relations_r.end(); ++itr){
				if(c.are_joinable(*itl, *itr, column)){
					l_relation = *itl;
					r_relation = *itr;
					joinable = true;
					break;
				}
			}
		}
		if(!joinable){
			return false;
		}
		cost = catalog::calculate_join_cost(*this, column, q, c, this->join_used, this->num_tuples, this->tuple_size, this->fr);
		materialization = catalog::materialization_cost(*this, c);
		this->cost_so_far = cost_l + cost_r + cost + materialization;
	}
	return true;
}

bool btree::is_selection(){
	if(this->is_node() && this->_node->op == SELECTION){
		return true;
	}
	return false;
}

bool btree::son_is_relation(tree_child son){
	if(!this->is_node())
		return false;
	btree *s;
	switch(son){
		case CHILD_LEFT:
			s = this->_node->left;
			break;
		default:
			s = this->_node->right;
	}
	if(!s->is_node())
		return true;
	return false;
}

btree& btree::get_son(tree_child son){
	if(this->is_node()){
		switch(son){
			case CHILD_LEFT:
				return *this->_node->left;
			default:
				return *this->_node->right;
		}
	}
	return *this;
}