OBJECTS=iterative_improvement.o tree.o catalog.o index.o query.o systemr.o btree.o
OBJECTSP3=iterative_improvement.o tree.o catalog.o index.o query.o systemr_wdata_sources.o btree.o mediator.o
FLAGS=-g -Wall

all: project1 project2 project3

verbose: FLAGS += -DVERBOSE
verbose: project1 project2 project3

clean:
	rm -rf $(OBJECTS) $(OBJECTSP3) project1 project2 project3 *~

project1: project1.cpp $(OBJECTS)
	g++ $(FLAGS) project1.cpp $(OBJECTS) -o project1 

project2: project2.cpp $(OBJECTS)
	g++ $(FLAGS) project2.cpp $(OBJECTS) -o project2

project3: project3.cpp $(OBJECTSP3)
	g++ $(FLAGS) project3.cpp $(OBJECTSP3) -o project3 

tree.o: tree.h tree.cpp
	g++ $(FLAGS) -c tree.cpp

query.o: query.h query.cpp
	g++ $(FLAGS) -c query.cpp

catalog.o: index.o catalog.h catalog.cpp
	g++ $(FLAGS) -c catalog.cpp

index.o: index.h index.cpp
	g++ $(FLAGS) -c index.cpp

systemr.o: systemr.h systemr.cpp
	g++ $(FLAGS) -c systemr.cpp

systemr_wdata_sources.o: systemr_wdata_sources.h systemr_wdata_sources.cpp
	g++ $(FLAGS) -c systemr_wdata_sources.cpp

mediator.o: mediator.cpp mediator.h
	g++ $(FLAGS) -c mediator.cpp

btree.o: catalog.h btree.h btree.cpp
	g++ $(FLAGS) -c btree.cpp

iterative_improvement.o: iterative_improvement.h iterative_improvement.cpp
	g++ $(FLAGS) -c iterative_improvement.cpp
