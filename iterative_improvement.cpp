#include <cstdlib>
#include "iterative_improvement.h"

// R join1 S -> R join2 S
void change_join(btree& plan){
	plan.set_random_join();
}

// R join S -> S join R
void conmutate_join(btree& plan){
	plan.random_conmutate();
}

// (R join S) join T -> R join (S join T) or R join (S join T) -> (R join S) join T
void associative_join(btree& plan){
	plan.random_associative();

}

// (R join S) join T -> (R join T) join S
void left_swap_join(btree& plan){
	plan.random_left_join();

}

// R join (S join T) -> S join (R join T)
void right_swap_join(btree& plan){
	plan.random_right_join();
}

typedef void (*mutate_operators_t)(btree&);

// Randomly searches for a neighbour plan
void search_neighbour(btree& plan){
	vector<mutate_operators_t> operators;
	operators.push_back(conmutate_join);
	operators.push_back(associative_join);
	operators.push_back(left_swap_join);
	operators.push_back(right_swap_join);
	operators.push_back(change_join);
	int choice = rand()%operators.size();
	operators[choice](plan);
}

// Checks if the tree is valid AND calculates cost
bool calculate_cost(btree& plan, query &q, catalog& c, unsigned long long &cost){
	bool valid = plan.calculate_cost(q, c);
	cost = plan.cost_so_far;
	return valid;
}

void iterative_improvement(catalog &c, query &q, unsigned long long cost, btree& plan, unsigned long long& final_cost){
	btree best_plan = plan;
	unsigned long long best_cost = cost;
	// Fixed number of iterations
	for(int iter = 0; iter < NUM_ITERATIONS; ++iter){
		btree new_plan;
		unsigned long long new_cost = 0;
		// Find a valid plan
		new_plan = best_plan;
		search_neighbour(new_plan);
		// if not valid, complete iteration
		if(!calculate_cost(new_plan, q, c, new_cost))
			continue;
		// if best, replace
		if(ITERATIVE_VERBOSE)
			cout << "best_plan(" << new_cost << "): " << best_plan << ", new_plan: " << new_plan << endl;
		if(new_cost < best_cost){
			best_cost = new_cost;
			best_plan = new_plan;
		}
	}
	plan = best_plan;
	final_cost = best_cost;
}
