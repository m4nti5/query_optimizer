#include "systemr_wdata_sources.h"
#include <climits>
#include <cassert>
#include <vector>
#include <deque>
#include <cstdlib>

void print_best_plans(deque<btree>& bp){
	deque<btree>::iterator it;
	for(it = bp.begin(); it != bp.end(); ++it){
		cout << (*it) << endl;
	}
}

void systemr_wdata_sources(mediator &m, query& query, btree& plan, unsigned long long& cost){
	// calculate best plan for 1 relation
	deque<btree> best_plans;
	for(int i = 0; i < query.num_relations(); ++i){
		// get relation i
		string ri = query.get_relation(i);
		// get possible selection for each field in query
		btree new_plan;
		if(set_selective_cost(ri, query, m, new_plan))
			best_plans.push_back(new_plan);
	}
	print_best_plans(best_plans);
	/*
	// calculate best plan for all relations
	// outer loop: check all 2 joins, all 3 joins, etc...
	for(int i = 1; i < query.num_relations(); ++i){
		// for all plans calculated
		int n = best_plans.size();
		for(int k = 0; k < n; ++k){
			btree current_plan = best_plans.front();
			best_plans.pop_front();
			// for all relations that can be joined
			for(int j = 0; j < query.num_relations(); ++j){
				string ri = query.get_relation(j);
				if(!current_plan.has_relation(ri)){
					btree new_plan;
					if(set_join_cost(current_plan, ri, query, m, new_plan))
						best_plans.push_back(new_plan);
				}
			}
		}
	}
	print_best_plans(best_plans);
	deque<btree>::iterator it = best_plans.begin();
	cost = (*it).cost_so_far;
	plan = (*it);
	it++;
	for(;it != best_plans.end(); ++it){
		if((*it).cost_so_far < cost){
			cost = (*it).cost_so_far;
			plan = (*it);
		}
	}
	*/
}

bool set_selective_cost(const string& relation, query &query, mediator &m, btree &t){
	data_source &md = m.get_data_source(relation);
	double total_fr = 1.0;
	string selection_used;
	int num_tuples, tuples_size;
	list<TABLE_OPERATION>::iterator begin, end;
	query.get_operation_info(relation, begin, end);
	list<int> selectable;
	if(!md.is_selection_possible(begin, end, selectable))
		return false;
	list<string> fields_added;
	unsigned long long cost = md.calculate_cost(t, selectable, selection_used, 
												fields_added, num_tuples, tuples_size);
	for(list<string>::iterator it = fields_added.begin(); it != fields_added.end(); ++it){
		t.add_field(*it);
	}
	btree l;
	l.set_relation(relation);
	t.set_operator(SELECTION);
	t.set_tree(l, CHILD_LEFT);
	t.fr = total_fr;
	t.relations_joined.push_back(relation);
	t.selection_used = selection_used;
	t.num_tuples = num_tuples;
	t.tuple_size = tuples_size;
	unsigned long long materialization = m.materialization_cost(t);
	// Cost of operation plus materialization
	t.cost_so_far = cost + materialization;
	return true;
}
/*
bool set_join_cost(btree &l, string relation, query &q, catalog &c, btree& new_tree){
	unsigned long long cost, materialization;
	list<TABLE_OPERATION>::iterator begin, end;
	q.get_operation_info(relation, begin, end);
	bool can_join = false;
	string column;
	// CHECK IF JOINABLE NOT BY CARTESIAN PRODUCT!!!
	for(; begin != end; ++begin){
		for(list<string>::iterator it = l.relations_joined.begin(); it != l.relations_joined.end(); ++it){
			if((*begin).v.table == *it){
				column = (*begin).column;
				can_join = true;
				break;
			}
		}
		if(can_join)
			break;
	}
	if(!can_join)
		return false;

	btree r;
	set_selective_cost(relation, q, c, r);

	new_tree.set_operator(JOIN);
	new_tree.set_tree(l, CHILD_LEFT);
	new_tree.set_tree(r, CHILD_RIGHT);
	new_tree.clear_fields();
	new_tree.add_field(column);
	cost = catalog::calculate_best_join_cost(l, r, column, q, c, new_tree.join_used, new_tree.num_tuples, new_tree.tuple_size, new_tree.fr);
	materialization = catalog::materialization_cost(new_tree, c);
	// cost of join plus cost of subtree operations plus materialization
	new_tree.cost_so_far = cost + l.cost_so_far + r.cost_so_far + materialization;
	new_tree.relations_joined.push_back(relation);
	return true;
}
*/