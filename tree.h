#ifndef TREE_H
#define TREE_H
#include <string>
#include <list>
#include <iostream>
#include "catalog.h"

using namespace std;

typedef enum operators{
	SELECTION,
	PROJECTION,
	JOIN,
	NONE
}operators;

typedef enum tree_child{
	CHILD_LEFT,
	CHILD_RIGHT
}tree_child;

class tree{
	public:
		/*
			BOOKKEPING FOR SYSTEMR
		*/
		long long cost_so_far;
		list<string> relations_joined;
		string join_used;
		string selection_used;
		long long num_tuples; // result cardinality
		long long tuple_size; // new tuple size, sum in case of join
		double fr;

		tree();
		tree(const tree&);
		~tree();
		bool is_ordered(string field, catalog &c);
		void get_fields(list<string>::const_iterator& init, list<string>::const_iterator& end) const;
		operators get_operator() const;
		string get_relation() const;
		string get_selection_relation() const;
		bool is_node();
		void add_field(string);
		void clear_fields();
		void set_fields(list<string>::const_iterator&, list<string>::const_iterator&);
		void set_operator(operators);
		void set_relation(string);
		void set_tree(tree &, tree_child);
		void set(tree*, tree_child);
		long long get_cost(catalog&);
		bool has_relation(string);
		ostream& print(ostream&);
		tree& operator=(const tree&);

	private:
		operators _operator;
		list<string> _fields;
		tree *_left;
		tree *_right;
		string _relation;
		void copy(tree*, const tree*);
};

inline ostream& operator <<(ostream& out, tree& t){
	return t.print(out);
}

#endif