#ifndef MEDIATOR_H
#define MEDIATOR_H
#include <string>
#include <list>
#include <vector>
#include <map>
#include "btree.h"

using namespace std;

class capacity{
	public:
		capacity();
		capacity(const capacity&);
		capacity& operator=(const capacity&);

		int first_tuple_cost;
		int page_cost;
		int tuples_number;
		int bytes_per_tuple;
		int number_of_pages;
		list<string> input;
		list<string> output;
};

class data_source{
	public:
		data_source();
		data_source(const data_source&);
		data_source& operator=(const data_source&);
		unsigned long long calculate_cost(btree& t, list<int>& selectables,
		 									 string &ds_used, list<string> &name_fields,
		 									 int &num_tuples, int &tuples_size);
		bool is_selection_possible(list<TABLE_OPERATION>::iterator &,
											 list<TABLE_OPERATION>::iterator &,
											 list<int> &);

		string name;
		string relation;
		vector<capacity> capacities;
};

class mediator{
	public:
		mediator();
		mediator(const mediator &);
		void add_mediator(const string& name, const string &relation);
		void add_capacity_data(const string& relation, int cap_index, int first_tuple_cost, int page_cost, int tuples_number, int bytes_per_tuple, int number_of_pages);
		void add_capacity_input(const string& relation, int cap_index, const string& field);
		void add_capacity_output(const string& relation, int cap_index, const string& field);
		bool is_join_possible(const string& r1, const string& r2, const string &column);
		bool is_djoin(const string& r1, const string& r2, const string &column);
		bool has_data_source(const string &r);
		data_source& get_data_source(const string &r);
		unsigned long long materialization_cost(btree& tree);
		static int page_size;

	private:
		map<string, data_source> mediator_list;
};

#endif