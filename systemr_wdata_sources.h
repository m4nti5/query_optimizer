#ifndef SYSTEMR_WDATA_SOURCES_H
#define SYSTEMR_WDATA_SOURCES_H
#include <list>
#include "btree.h"
#include "query.h"
#include "index.h"
#include "mediator.h"

using namespace std;

/**
	Analizes the tree t as a node with selection and a relation,
	calculates the best cost in selection based in the info in the mediators.
	Used for first step in systemr algorithm
*/

bool set_selective_cost(const string& relation, query &query, mediator &m, btree &result);

/**
	Calculates best posible join plan from the options
*/
bool set_join_cost(btree &left, const string& relation, query &q, mediator &m, btree &result);

void systemr_wdata_sources(mediator& m, query& query, btree& plan, unsigned long long& cost);

#endif