#ifndef ITERATIVE_IMPROVEMENT_H
#define ITERATIVE_IMPROVEMENT_H
#include "btree.h"
#include "query.h"
#include "catalog.h"
#ifdef VERBOSE
#define ITERATIVE_VERBOSE false
#else
#define ITERATIVE_VERBOSE false
#endif

#define NUM_ITERATIONS 10000

void iterative_improvement(catalog &c, query &q, unsigned long long cost, btree& plan, unsigned long long& final_cost);

#endif
