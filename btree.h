#ifndef BTREE_H
#define BTREE_H
#include <iostream>
#include <string>
#include <list>
#include "catalog.h"
#ifdef VERBOSE
#define PRINT_VERBOSE true
#else
#define PRINT_VERBOSE false
#endif

using namespace std;

typedef enum operators{
	SELECTION,
	PROJECTION,
	JOIN,
	NONE
}operators;

typedef enum tree_child{
	CHILD_LEFT,
	CHILD_RIGHT
}tree_child;

class query;

class btree{
	public:
		/*
			BOOKKEPING FOR SYSTEMR
		*/
		unsigned long long cost_so_far;
		list<string> relations_joined;
		string join_used;
		string selection_used;
		unsigned long long num_tuples; // result cardinality
		unsigned long long tuple_size; // new tuple size, sum in case of join
		double fr;
		void copy_systemr_info(const btree &t);

		btree();
		btree(const btree&);
		~btree();
		bool conmutate();
		bool associative();
		bool backward_associative();
		bool left_join();
		bool right_join();
		bool random_conmutate();
		bool random_associative();
		bool random_left_associative();
		bool random_right_associative();
		bool random_left_join();
		bool random_right_join();
		void set_random_join();
		bool is_left_join();
		bool is_right_join();
		bool is_ordered(string field, catalog &c);
		void get_fields(list<string>::const_iterator& init, list<string>::const_iterator& end) const;
		operators get_operator() const;
		string get_relation() const;
		string get_selection_relation();
		void get_relations(list<string>&);
		string get_selection_relation() const;
		bool is_node();
		void add_field(string);
		void clear_fields();
		void set_fields(list<string>::const_iterator&, list<string>::const_iterator&);
		void set_operator(operators);
		void set_relation(string);
		void set_tree(btree &, tree_child);
		void set(btree*, tree_child);
		bool has_relation(string);
		bool is_selection();
		bool is_relation();
		ostream& print(ostream&);
		btree& operator=(const btree&);
		bool calculate_cost(query &, catalog&);
		bool son_is_relation(tree_child);
		btree& get_son(tree_child);

	private:
		class node;
		class leaf;
		node *_node;
		leaf *_leaf;
		void copy(node *src, node *&dst);
		void copy(btree *src, btree *&dst);
		void copy(leaf *src, leaf *&dst);
		void free(node *&);
		void free(leaf *&);
		void free(btree *&);
		void get_random_node(btree *&);
		void get_all_nodes(map<int, btree*>& nodes);
		bool has_left_join();
		bool has_right_join();
};

inline ostream& operator <<(ostream& out, btree& b){
	return b.print(out);
}

class btree::node{
	public:
		operators op;
		list<string> fields;
		btree *left;
		btree *right;
		node(operators);
		node();
		ostream& print(ostream&, btree&);

	private:
		void print_fields(ostream &out, list<string>&);
};

class btree::leaf{
	public:
		string relation;
		leaf();
		leaf(string);
};
#endif
