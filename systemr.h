#ifndef SYSTEMR_H
#define SYSTEMR_H
#include <list>
#include "btree.h"
#include "query.h"
#include "index.h"
#include "catalog.h"

using namespace std;

/**
	Analizes the tree t as a node with selection and a relation,
	calculates the best cost in selection based in the trees in the catalog.
	Used for first step in systemr algorithm
*/

void set_selective_cost(string relation, query &query, catalog &c, btree &result);

/**
	Calculates best posible join plan from the options
*/
bool set_join_cost(btree &left, string relation, query &q, catalog &c, btree &result);

void systemr(catalog& c, query& query, btree& plan, unsigned long long& cost);

#endif