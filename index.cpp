#include "index.h"

index::index(){
	_type = BTREE;
	_order = UNCLUSTERED;
	_number_keys = _max_value = _min_value = _num_pages = _tuples_per_pages = 0;
}

index_type index::get_type()const{
	return this->_type;
}

index& index::operator=(const index& i){
	set_type(i.get_type());
	set_order(i.get_order());
	list<string>::const_iterator begin, end;
	i.get_fields(begin, end);
	set_fields(begin, end);
	set_number_keys(i.get_number_keys());
	set_max_value(i.get_max_value());
	set_min_value(i.get_min_value());
	set_num_pages(i.get_num_pages());
	set_tuples_per_pages(i.get_tuples_per_pages());
	return *this;
}

index_order index::get_order()const{
	return this->_order;
}

string index::get_first_field(){
	list<string>::iterator it = this->_fields.begin();
	if(it == this->_fields.end())
		return "";
	return *(it);
}

void index::get_fields(list<string>::const_iterator& begin, list<string>::const_iterator& end)const{
	begin = this->_fields.begin();
	end = this->_fields.end();
}

int index::get_number_keys()const{
	return this->_number_keys;
}

int index::get_max_value()const{
	return this->_max_value;
}

int index::get_min_value()const{
	return this->_min_value;
}

int index::get_num_pages()const{
	return this->_num_pages;
}

int index::get_tuples_per_pages()const{
	return this->_tuples_per_pages;
}

void index::add_field(string f){
	this->_fields.push_back(f);
}
	
void index::set_type(index_type it){
	this->_type = it;
}

void index::set_order(index_order io){
	this->_order = io;
}

void index::set_fields(list<string>::const_iterator& begin, list<string>::const_iterator& end){
	for(; begin != end; ++begin)
		this->add_field(*begin);
}

void index::set_number_keys(int nk){
	this->_number_keys = nk;
}

void index::set_max_value(int mv){
	this->_max_value = mv;
}

void index::set_min_value(int iv){
	this->_min_value = iv;
}

void index::set_num_pages(int np){
	this->_num_pages = np;
}

void index::set_tuples_per_pages(int tpp){
	this->_tuples_per_pages = tpp;
}

ostream& index::print(ostream& out){
	out << this->_type << ", " << this->_order << endl;
	out << "\tnumber of keys: " << this->_number_keys << endl;
	out << "\tmax value: " << this->_max_value << endl;
	out << "\tmin value: " << this->_min_value << endl;
	out << "\tnumber of pages: " << this->_num_pages << endl;
	out << "\ttuples per pages: " << this->_tuples_per_pages << endl;
	out << "\tfields: ";
	bool first = true;
	for(list<string>::iterator it = this->_fields.begin(); it != this->_fields.end(); ++it){
		if(first){
			first = false;
		}else{
			out << ", ";
		}
		out << *it;
	}
	return out << endl;
}
