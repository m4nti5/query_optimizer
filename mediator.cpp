#include "mediator.h"
#include <sstream>
#include <string>
#include <map>
#include <list>
#include <vector>

int mediator::page_size = 5000;
capacity::capacity(){
	this->first_tuple_cost = 0;
	this->page_cost = 0;
	this->tuples_number = 0;
	this->bytes_per_tuple = 0;
	this->number_of_pages = 0;
}

capacity::capacity(const capacity& ds){
	(*this) = ds;
}

capacity& capacity::operator=(const capacity& ds){
	this->input.clear();
	this->output.clear();

	for(list<string>::const_iterator it = ds.input.begin(); it != ds.input.end(); ++it){
		this->input.push_back(*it);
	}

	for(list<string>::const_iterator it = ds.output.begin(); it != ds.output.end(); ++it){
		this->output.push_back(*it);
	}

	this->first_tuple_cost = ds.first_tuple_cost;
	this->page_cost = ds.page_cost;
	this->tuples_number = ds.tuples_number;
	this->bytes_per_tuple = ds.bytes_per_tuple;
	this->number_of_pages = ds.number_of_pages;
	return *this;
}

data_source::data_source(){
	this->name = "";
	this->relation = "";
}

data_source::data_source(const data_source& d){
	*this = d;
}

data_source& data_source::operator=(const data_source& m){
	this->capacities.clear();

	this->name = m.name;
	this->relation = m.relation;

	for(vector<capacity>::const_iterator it = m.capacities.begin(); it != m.capacities.end(); ++it){
		this->capacities.push_back(*it);
	}
	return *this;
}

bool data_source::is_selection_possible(list<TABLE_OPERATION>::iterator &begin,
									list<TABLE_OPERATION>::iterator &end,
									list<int>& selectable){
	for(int i = 0; i < this->capacities.size(); ++i){
		list<string>::iterator inputs_r1 = this->capacities[i].input.begin();
		bool all = true;
		for(; inputs_r1 != this->capacities[i].input.end(); ++inputs_r1){
			bool found = false;
			list<TABLE_OPERATION>::iterator begin_copy = begin;
			for(; begin_copy != end; ++begin_copy){
				if((*begin_copy).v.type == CONSTANT && (*inputs_r1) == (*begin_copy).column){
					found = true;
					break;
				}
			}
			if(!found)
				all = false;
		}
		if(all){
			selectable.push_back(i);
		}
	}
	return true;
}


unsigned long long mediator::materialization_cost(btree& tree){
	double tuple_per_page_rate = tree.tuple_size / mediator::page_size;
	unsigned long long materialization = tree.num_tuples * tuple_per_page_rate;
	if(materialization <= 0)
		materialization = 1;
	return materialization;
}

unsigned long long data_source::calculate_cost(btree& t, list<int>& selectables,
		 									 string &ds_used, list<string> &name_fields,
		 									 int &num_tuples, int &tuples_size){
	unsigned long long cost = 0;
	bool capacity_used = false;
	stringstream ss;

	for(int i = 0; i < selectables.size(); ++ i){
		cost += this->capacities[i].number_of_pages + this->capacities[i].first_tuple_cost +
			 (this->capacities[i].number_of_pages * this->capacities[i].page_cost);
		num_tuples += this->capacities[i].number_of_pages;
		ss << " " << i;
		if(tuples_size < this->capacities[i].bytes_per_tuple)
			tuples_size = this->capacities[i].bytes_per_tuple;
		capacity_used = true;
		list<string>::iterator input = this->capacities[i].input.begin();
		for(; input != this->capacities[i].input.end(); ++input){
			name_fields.push_back(*input);
		}
		list<string>::iterator output = this->capacities[i].output.begin();
		for(; output != this->capacities[i].output.end(); ++output){
			name_fields.push_back(*output);
		}
	}
	if(capacity_used)
		ds_used = this->name + ss.str();
	return cost;
}

mediator::mediator(){

}

mediator::mediator(const mediator &m){
	this->mediator_list.clear();
	for(map<string, data_source>::const_iterator it = m.mediator_list.begin(); it != m.mediator_list.end(); ++it)
		this->mediator_list[it->first] = it->second;
}

void mediator::add_mediator(const string& name, const string &relation){
	data_source m;
	m.name = name;
	this->mediator_list[relation] = m;
}

void mediator::add_capacity_data(const string& relation, int cap_index, int first_tuple_cost, 
						int page_cost, int tuples_number, int bytes_per_tuple, int number_of_pages){

	this->mediator_list[relation].capacities[cap_index].first_tuple_cost = first_tuple_cost;
	this->mediator_list[relation].capacities[cap_index].page_cost = page_cost;
	this->mediator_list[relation].capacities[cap_index].tuples_number = tuples_number;
	this->mediator_list[relation].capacities[cap_index].bytes_per_tuple = bytes_per_tuple;
	this->mediator_list[relation].capacities[cap_index].number_of_pages = number_of_pages;
}

void mediator::add_capacity_input(const string& relation, int ds_idx, const string& field){
	if(this->mediator_list.find(relation) == this->mediator_list.end())
		return;
	this->mediator_list[relation].capacities[ds_idx].input.push_back(field);
}

void mediator::add_capacity_output(const string& relation, int ds_idx, const string& field){
	if(this->mediator_list.find(relation) == this->mediator_list.end())
		return;
	this->mediator_list[relation].capacities[ds_idx].output.push_back(field);
}
/*
bool mediator::is_join_possible(const string& r1, const string& r2, const string &column){
	map<string, mediator>::iterator it_r1 = this->mediator_list.find(r1), it_r2 = this->mediator_list.find(r2);
	if(it_r1 == this->mediator_list.end()
		|| it_r2 == this->mediator_list.end())
		return false;
	list<string>::iterator inputs_r1 = it_r1->capacities.begin(), inputs_r2 = it_r2->capacities.begin();
	bool found = false;
	for(; inputs_r1 != it_r1->capacities.end(); ++it_r1){
		if((*inputs_r1) == column){
			found = true;
			break;
		}
	}
	if(!found)
		return false;
	found = false;
	for(; inputs_r2 != it_r2->capacities.end(); ++it_r2){
		if((*inputs_r2) == column){
			found = true;
			break;
		}
	}
	return found;
}
*/
bool mediator::has_data_source(const string &r){
	return this->mediator_list.find(r) == this->mediator_list.end();
}

data_source& mediator::get_data_source(const string &r){
	return this->mediator_list[r];
}