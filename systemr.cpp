#include "systemr.h"
#include <climits>
#include <cassert>
#include <vector>
#include <deque>
#include <cstdlib>

void print_best_plans(deque<btree>& bp){
	deque<btree>::iterator it;
	for(it = bp.begin(); it != bp.end(); ++it){
		cout << (*it) << endl;
	}
}

void systemr(catalog& c, query& query, btree& plan, unsigned long long& cost){
	// calculate best plan for 1 relation
	deque<btree> best_plans;
	for(int i = 0; i < query.num_relations(); ++i){
		// get relation i
		string ri = query.get_relation(i);
		btree new_plan;
		set_selective_cost(ri, query, c, new_plan);
		best_plans.push_back(new_plan);
	}
	print_best_plans(best_plans);
	
	// calculate best plan for all relations
	// outer loop: check all 2 joins, all 3 joins, etc...
	for(int i = 1; i < query.num_relations(); ++i){
		// for all plans calculated
		int n = best_plans.size();
		for(int k = 0; k < n; ++k){
			btree current_plan = best_plans.front();
			best_plans.pop_front();
			// for all relations that can be joined
			for(int j = 0; j < query.num_relations(); ++j){
				string ri = query.get_relation(j);
				if(!current_plan.has_relation(ri)){
					btree new_plan;
					if(set_join_cost(current_plan, ri, query, c, new_plan))
						best_plans.push_back(new_plan);
				}
			}
		}
	}
	print_best_plans(best_plans);
	deque<btree>::iterator it = best_plans.begin();
	cost = (*it).cost_so_far;
	plan = (*it);
	it++;
	for(;it != best_plans.end(); ++it){
		if((*it).cost_so_far < cost){
			cost = (*it).cost_so_far;
			plan = (*it);
		}
	}
}


// FOR NOT CONSIDERED: SAME COLUMN MULTIPLE TIMES IN QUERY, in ex. t1.c1 < 10 AND t1.c1 > t2.c2
void set_selective_cost(string relation, query &query, catalog &c, btree &t){
	list<TABLE_OPERATION>::iterator begin, end;
	string selection_used = "scan";
	assert(c.has_table(relation));
	tableinfo &info = c.get_tableinfo(relation);
	unsigned long long cost = catalog::calculate_scan_cost(info);
	query.get_operation_info(relation, begin, end);
	double total_fr = 1.0;
	if(begin != end){
		for(; begin != end; begin++){
			// check if there is an index
			if((*begin).v.type == CONSTANT){
			 	if(info.indexes.size() != 0){
					for(list<index>::iterator it = info.indexes.begin(); it != info.indexes.end(); ++it){
						list<string>::const_iterator f_begin, f_end;
						(*it).get_fields(f_begin, f_end);
						//for(; f_begin != f_end; ++f_begin){
							if((*f_begin) == (*begin).column){
								total_fr = catalog::calculate_fr(c, info, *begin, (*it));
								unsigned long long new_cost = catalog::calculate_cost(info, *it, total_fr, true);
								if(new_cost < cost){
									cost = new_cost;
									selection_used = "index " + (*it).id;
								}
							}else{
								unsigned long long new_cost = catalog::calculate_cost(info, *it, 0.0, false);
								if(new_cost < cost){
									cost = new_cost;
									selection_used = "index " + (*it).id;
								}
							}
						//}
					}
				}
				if(selection_used != "scan")
					t.add_field((*begin).column);
			}
		}
	}
	if(selection_used != "scan"){
		btree l;
		l.set_relation(relation);
		t.set_operator(SELECTION);
		t.set_tree(l, CHILD_LEFT);
		t.fr = total_fr;
		t.relations_joined.push_back(relation);
		t.selection_used = selection_used;
		t.num_tuples = info.tuples_number * total_fr;
		t.tuple_size = info.tuples_size;
		unsigned long long materialization = catalog::materialization_cost(t, c);
		// Cost of operation plus materialization
		t.cost_so_far = cost + materialization;
	}else{
		t.fr = total_fr;
		t.cost_so_far = 0;
		t.relations_joined.push_back(relation);
		t.selection_used = selection_used;
		t.num_tuples = info.tuples_number * total_fr;
		t.tuple_size = info.tuples_size;
		t.set_relation(relation);
	}
}

bool set_join_cost(btree &l, string relation, query &q, catalog &c, btree& new_tree){
	unsigned long long cost, materialization;
	list<TABLE_OPERATION>::iterator begin, end;
	q.get_operation_info(relation, begin, end);
	bool can_join = false;
	string column;
	// CHECK IF JOINABLE NOT BY CARTESIAN PRODUCT!!!
	for(; begin != end; ++begin){
		for(list<string>::iterator it = l.relations_joined.begin(); it != l.relations_joined.end(); ++it){
			if((*begin).v.table == *it){
				column = (*begin).column;
				can_join = true;
				break;
			}
		}
		if(can_join)
			break;
	}
	if(!can_join)
		return false;

	btree r;
	set_selective_cost(relation, q, c, r);

	new_tree.set_operator(JOIN);
	new_tree.set_tree(l, CHILD_LEFT);
	new_tree.set_tree(r, CHILD_RIGHT);
	new_tree.clear_fields();
	new_tree.add_field(column);
	cost = catalog::calculate_best_join_cost(l, r, column, q, c, new_tree.join_used, new_tree.num_tuples, new_tree.tuple_size, new_tree.fr);
	materialization = catalog::materialization_cost(new_tree, c);
	// cost of join plus cost of subtree operations plus materialization
	new_tree.cost_so_far = cost + l.cost_so_far + r.cost_so_far + materialization;
	new_tree.relations_joined.push_back(relation);
	return true;
}
